import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

buildscript {
  repositories {
    mavenCentral()
    maven {
      url = uri("https://plugins.gradle.org/m2/")
    }
  }
}

//TODO: Remove in future Idea versions when the bug is fixed
@Suppress(
  "DSL_SCOPE_VIOLATION"
)
plugins {
  id("maven-publish")
  id("java")
  id("io.spring.dependency-management") version ("1.0.9.RELEASE")
  kotlin("jvm") version(libs.versions.kotlin)
  kotlin("plugin.serialization") version(libs.versions.kotlin)
  id("com.github.johnrengelman.shadow") version("6.0.0")
}

group = "com.serverless"
version = "dev"

description = """hello"""

java.sourceCompatibility = JavaVersion.VERSION_11
java.targetCompatibility = JavaVersion.VERSION_11

kotlin {
  tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions.jvmTarget = "11"
  }
}

tasks.withType<JavaCompile> {
  options.encoding = "UTF-8"
}

repositories {
  mavenCentral()
}

dependencyManagement {
    imports {
        mavenBom("com.amazonaws:aws-java-sdk-bom:1.11.688")
        mavenBom("com.amazonaws:aws-xray-recorder-sdk-bom:2.4.0")
        mavenBom("software.amazon.awssdk:bom:2.10.73")
    }
}

dependencies {
  api(libs.bundles.apis)
  implementation(libs.bundles.aws)
}

task<Exec>("deploy") {
  group = "serverless"
  commandLine("serverless", "deploy")
}

task<Exec>("remove") {
  group = "serverless"
  commandLine("serverless", "remove")
}

val shadowJar: ShadowJar by tasks
shadowJar.apply {
  transform(com.github.jengelman.gradle.plugins.shadow.transformers.Log4j2PluginsCacheFileTransformer::class.java)
}

val build: Task = tasks.getByName("build")
build.finalizedBy(shadowJar)
