# AWS Serverless Demo

Example includes:
- Lambda
- SQS
- DynamoDB
- API Gateway
- Cognito UserPools
- X-Ray
- CloudWatch Logs

## Prerequisites

### Run at Home
You need to have `Java 11`, `NodeJs 10+` and `Serverless` Framework.

You need to have programmatic access keys configured for a user with either an `arn:aws:iam::aws:policy/AdministratorAccess` policy,
or a custom policy provided in the `ita-lambda-example-dev-us-east-1-policy.json` file (which is still permits more than it should :) )

### Run on Company account

Go to [EC2 Console](https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#Home:) and lick **Launch instance**.

![UserPools](img/ec2_1.png)

Select Amazon Linux 2 AMI.

![UserPools](img/ec2_2.png)

Select `t2.small` instance type, as micro won't have enough resources to compile code.

![UserPools](img/ec2_3.png)

Click configure instance details.
On the next screen choose an `Training-ITADeployInstanceRole` as instance **IAM Role**. Leave rest as default.

![UserPools](img/ec2_4.png)

Click next till you reach Security groups section. Configure SSH access only from your ip.

![UserPools](img/ec2_5.png)

When your instance is running, click connect button and follow the instructions to SSH to the instance.

![UserPools](img/ec2_6.png)

Inside EC2 instance install the following packages:

```bash
sudo yum install java-11-amazon-corretto-headless git gcc-c++ make -y
curl -sL https://rpm.nodesource.com/setup_14.x | sudo -E bash -
sudo yum install -y nodejs
sudo npm i -g serverless
```

## Setup

Clone the repository.

```bash
git clone https://gitlab.com/DmyMi/ita-lambda-demo
```

Update the `DOMAIN_SUFFIX` value in the provider environment section of `serverless.yml` file to some unique value.
For example
```yaml
provider:
# other values
  environment:
    DOMAIN_SUFFIX: fancy-auth
```

## Build

```bash
./gradlew clean build
```

## deploy

```bash
./gradlew deploy
```

## Access

After the deployment completes, you should see two API endpoints in the output

```yaml
endpoints:
  POST - https://RANDOM_STRING.execute-api.us-east-1.amazonaws.com/v1/message
  GET - https://RANDOM_STRING.execute-api.us-east-1.amazonaws.com/v1/message
functions:
  ping: ita-lambda-example-dev-ping
  pong: ita-lambda-example-dev-pong
  getMessage: ita-lambda-example-dev-getMessage
layers:
  None
```

If you try to get the messages (replacing RANDOM_STRING with your value)

```bash
curl -v https://RADNOM_STRING.execute-api.us-east-1.amazonaws.com/v1/message
```

You will get `{"message":"Unauthorized"}`. This means these endpoints are protected and will only work with a valid JSON Web Token.

Log into the AWS Console and navigate to the [Cognito Dashboard](https://console.aws.amazon.com/cognito/home?region=us-east-1).
Make sure you're in the same region you deployed your service to and click Manage User Pools:

![UserPools](img/cognito1.png)

From there, click on the user pool that was created:

![Pool](img/cognito2.png)

Then find to the "App Client Settings" link in the left menu:
![App](img/cognito3.png)

In App Client Settings scroll down to the "Launch Hosted UI" button:
![App](img/cognito4.png)

Click it to launch the sign in UI. As we don't have an account yet - click `Sign up`.
![App](img/cognito5.png)

Enter a valid e-mail address to receive the verification code and enter it in the final screen.
![App](img/cognito6.png)

After you sign up, you should be redirected to a non-existing localhost page.
Copy the URL. It should look something like this:

![App](img/cognito7.png)

That URL contains two JSON web tokens, an `id_token` and an `access_token`.
They serve different purposes, but either can be used in this case to authorize requests to the API.

Copy the `id_token` which is located after the `id_token=` and before the `&access_token`. You can use your favourite text editor to find and required parts.

Next, go to the terminal and run the following to post new message (replace `RANDOM_STRING` with your actual value)
```bash
export TOKEN=YOUR_ID_TOKEN_HERE
export URL=https://RANDOM_STRING.execute-api.us-east-1.amazonaws.com/v1/message
curl -v -H "Authorization: Bearer ${TOKEN}" ${URL} -d '{"message": "Demo message"}'
```

If everything is fine - you should receive a successful response: `{"message":"Your message was sent successfully!", ...}`

Try getting the messages:
```bash
curl -v -H "Authorization: Bearer ${TOKEN}" ${URL}
```

## clean up

```bash
./gradlew remove
```