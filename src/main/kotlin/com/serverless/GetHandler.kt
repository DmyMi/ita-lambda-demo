package com.serverless

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder
import com.amazonaws.services.dynamodbv2.model.ScanRequest
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestHandler
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyRequestEvent
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyResponseEvent
import com.serverless.api.GetResponse
import com.serverless.db.Message
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.apache.http.HttpStatus
import org.apache.logging.log4j.LogManager

class GetHandler : RequestHandler<APIGatewayV2ProxyRequestEvent, APIGatewayV2ProxyResponseEvent> {
    override fun handleRequest(input: APIGatewayV2ProxyRequestEvent, context: Context): APIGatewayV2ProxyResponseEvent {
        /*
        Create a DynamoDB client
         */
        val dynamoDBClient = AmazonDynamoDBClientBuilder.defaultClient()
        /*
        Create a scan request without any filters to return an entire table
        and execute it
         */
        val request = ScanRequest().withTableName("message_table")
        val result = dynamoDBClient.scan(request)
        /*
        To simplify the transformation of DynamoDB data we simply take the resulting Map<String, AttributeValue>
        and check for required fields, knowing that their type is String we use the getS() property getter.
        If for some reason we get a null value - we use empty string as default.
         */
        val messages = result.items.map {
            val id = it["id"]?.s ?: ""
            val message = it["message"]?.s ?: ""
            val user = it["user"]?.s ?: ""
            Message(id = id, user = user, message = message)
        }
        LOG.info("Got ${messages.size} messages")
        /*
        Return success
         */
        return APIGatewayV2ProxyResponseEvent().apply {
            isIsBase64Encoded = false
            statusCode = HttpStatus.SC_OK
            body = Json.encodeToString(GetResponse(messages))
            headers = mapOf("X-Powered-By" to "AWS Lambda")
        }
    }

    companion object {
        private val LOG = LogManager.getLogger(GetHandler::class.java)
    }
}