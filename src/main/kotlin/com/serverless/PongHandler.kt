package com.serverless

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder
import com.amazonaws.services.dynamodbv2.document.DynamoDB
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec
import com.amazonaws.services.dynamodbv2.model.AmazonDynamoDBException
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException
import com.amazonaws.services.dynamodbv2.model.PutItemRequest
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestHandler
import com.amazonaws.services.lambda.runtime.events.SQSEvent
import com.serverless.db.Message
import com.serverless.db.toItem
import kotlinx.serialization.SerializationException
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.apache.logging.log4j.LogManager
import java.util.*

class PongHandler : RequestHandler<SQSEvent, Unit> {
    override fun handleRequest(input: SQSEvent, context: Context) {
        /*
        Create a DynamoDB client
         */
        val dynamoDBClient = AmazonDynamoDBClientBuilder.defaultClient()

        /*
        As the event can have multiple messages delivered at a time we need to iterate over them
         */
        input.records.forEach {
            try {
                /*
                Deserialize the message payload and create a DynamoDB PUT request
                providing the table name and an item to save.
                Just to simplify the lowest level client is used that takes
                a Map<String, AttributeValue> as an input.
                Where AttributeValue is a special type that has a data value and dynamoDB value type specified
                 */
                val msg = Json.decodeFromString<Message>(it.body)
                val request = PutItemRequest()
                    .withTableName("message_table")
                    .withItem(msg.toItem())
                /*
                Execute the request and check if we use too much WCUs :)
                 */
                dynamoDBClient.putItem(request).apply {
                    LOG.info("This request consumed ${consumedCapacity?.capacityUnits}")
                }
            } catch (e: AmazonDynamoDBException) {
                LOG.error("Failed to put with: ${e.stackTrace}")
            } catch (e: SerializationException) {
                LOG.error("failed to deserialize object: $e")
            }
        }
    }

    companion object {
        private val LOG = LogManager.getLogger(PongHandler::class.java)
    }
}