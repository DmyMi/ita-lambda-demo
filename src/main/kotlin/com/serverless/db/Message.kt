package com.serverless.db

import kotlinx.serialization.Serializable

@Serializable
data class Message(
    val message: String,
    var user: String = "",
    val id: String = ""
)