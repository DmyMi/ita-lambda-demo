package com.serverless.api

import kotlinx.serialization.Serializable

@Serializable
data class PingResponse(val message: String, val output: Output? = null)

@Serializable
data class Output(val message: String, val messageId: String)
