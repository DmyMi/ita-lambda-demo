package com.serverless.api

import kotlinx.serialization.Serializable

@Serializable
data class Token(
    val jwt: Jwt
)

@Serializable
data class Jwt(
    val claims: Claims
)

// much more fields in the claim, but need only sub (cognitoId) for demo
@Serializable
data class Claims(
    val sub: String
)