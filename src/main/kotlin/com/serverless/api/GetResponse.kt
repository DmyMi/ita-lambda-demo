package com.serverless.api

import com.serverless.db.Message
import kotlinx.serialization.Serializable

@Serializable
data class GetResponse(val messages: List<Message>)