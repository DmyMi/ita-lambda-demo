package com.serverless

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestHandler
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyRequestEvent
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyResponseEvent
import com.amazonaws.services.sqs.AmazonSQSClientBuilder
import com.amazonaws.services.sqs.model.SendMessageRequest
import com.serverless.api.Output
import com.serverless.api.PingResponse
import com.serverless.api.Token
import com.serverless.db.Message
import com.serverless.db.toJsonElement
import kotlinx.serialization.SerializationException
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromJsonElement
import org.apache.http.HttpStatus
import org.apache.logging.log4j.LogManager
import java.util.*

class PingHandler : RequestHandler<APIGatewayV2ProxyRequestEvent, APIGatewayV2ProxyResponseEvent> {

    override fun handleRequest(input: APIGatewayV2ProxyRequestEvent, context: Context): APIGatewayV2ProxyResponseEvent {
        /*
        Parse a JSON message from API Gateway.
        Check if it is Base64 encoded by API gateway and decode it if needed.
         */
        val request = try {
            if (input.isIsBase64Encoded) Json.decodeFromString<Message>(String(Base64.getDecoder().decode(input.body)))
            else Json.decodeFromString<Message>(input.body)
        } catch (e: SerializationException) {
            LOG.error("failed to deserialize object: $e")
            throw RuntimeException(e)
        }
        /*
        As the parsed JWT token comes as a Map<String, Any> - convert the map to a JsonElement
        using a custom extension function
         */
        val json = input.requestContext.authorizer.toJsonElement()
        /*
        Deserialize the JSON into an object
        As there are many keys in the claim, we ignore unknown and only take the cognitoId
         */
        val token = Json { ignoreUnknownKeys = true }.decodeFromJsonElement<Token>(json)
        request.user = token.jwt.claims.sub

        LOG.info("received: $request")

        val msg = request.message

        return if (msg.isBlank()) {
            LOG.info("No message found :(")
            /*
            If user did not provide a message - return an error
             */
            APIGatewayV2ProxyResponseEvent().apply {
                isIsBase64Encoded = false
                statusCode = HttpStatus.SC_BAD_REQUEST
                body = Json.encodeToString(PingResponse("You have failed! Please retry"))
                headers = mapOf("X-Powered-By" to "AWS Lambda")
            }
        } else {
            /*
            If there is a message - create SQS client and get a queue url by name
            (hard coded for this example)
             */
            val sqs = AmazonSQSClientBuilder.defaultClient()
            val queueUrl = sqs.getQueueUrl("ExampleQueue").queueUrl
            /*
            Send message to queue. Message is a serialized object we got from user plus his cognitoId
             */
            val msgRequest = SendMessageRequest()
                .withQueueUrl(queueUrl)
                .withMessageBody(Json.encodeToString(request))
            val result = sqs.sendMessage(msgRequest)

            LOG.info("Successfully sent message with id: ${result.messageId}")
            /*
            Return success
             */
            APIGatewayV2ProxyResponseEvent().apply {
                isIsBase64Encoded = false
                statusCode = HttpStatus.SC_OK
                body = Json.encodeToString(
                    PingResponse(
                        "Your message was sent successfully!",
                        Output(msg, result.messageId)
                    )
                )
                headers = mapOf("X-Powered-By" to "AWS Lambda")
            }
        }
    }

    companion object {
        private val LOG = LogManager.getLogger(PingHandler::class.java)
    }
}
